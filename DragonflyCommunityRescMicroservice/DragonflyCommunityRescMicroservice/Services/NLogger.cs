﻿using System;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace DragonflyCommunityRescMicroservice.Services
{
    public class NLogger
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        public NLogger(string filename)
        {
            var config = new LoggingConfiguration();
            var logFile = new FileTarget("logfile")
                {FileName = $"Logs\\{filename}-{DateTime.Today:MM-dd-yy}.log"};
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            LogManager.Configuration = config;
        }

        public void WriteError(string error)
        {
            _logger.Error(error);
        }

        public void WriteInfo(string info)
        {
            _logger.Info(info);
        }
    }
}
