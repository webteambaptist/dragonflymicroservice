﻿using System;
using System.Collections.Generic;
using System.Linq;
using DragonflyCommunityRescMicroservice.Models;
using Newtonsoft.Json;

namespace DragonflyCommunityRescMicroservice.Services
{
    public class Entity
    {
        private readonly DragonflyContext _context;
        private readonly NLogger _logger;

        public Entity(DragonflyContext context)
        {
            _context = context;
            _logger = new NLogger("Entity");
        }
        /// <summary>
        /// Gets All Ages from DB
        /// </summary>
        /// <returns>List of Ages</returns>
        public List<Age> GetAges()
        {
            try
            {
                return _context.Ages.OrderBy(x=>x.Age1).ToList();
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: GetAges() :: {e.Message}");
                throw;
            }
        }
        /// <summary>
        /// Gets all Insurances from DB
        /// </summary>
        /// <returns>List of Insurances</returns>
        public List<Insurance> GetInsurances()
        {
            try
            {
                return _context.Insurances.OrderBy(x=>x.Name).ToList();
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: GetInsurances() :: {e.Message}");
                throw;
            }
        }
        /// <summary>
        /// Gets all Languages from DB
        /// </summary>
        /// <returns>List of Languages</returns>
        public List<Language> GetLanguages()
        {
            try
            {
                return _context.Languages.OrderBy(x=>x.Language1).ToList();
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: GetLanguages() :: {e.Message}");
                throw;
            }
        }
        /// <summary>
        /// Gets all Locations from DB
        /// </summary>
        /// <returns>List of Locations</returns>
        public List<Location> GetLocations()
        {
            try
            {
                return _context.Locations.OrderBy(x=>x.Location1).ToList();
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: GetLocations() :: {e.Message}");
                throw;
            }
        }
        /// <summary>
        /// Gets all Services from DB
        /// </summary>
        /// <returns>List of Services</returns>
        public List<Service> GetServices()
        {
            try
            {
                return _context.Services.OrderBy(x=>x.Service1).ToList();
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: GetServices() :: {e.Message}");
                throw;
            }
        }
        /// <summary>
        /// Gets Resource from DB 
        /// </summary>
        /// <param name="id">Resource Id</param>
        /// <returns>Resource</returns>
        public Resource GetResourceById(int id)
        {
            try
            {
                return _context.Resources.FirstOrDefault(x => x.Id == id);
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception getting Resource for ({id}) :: GetResourceById() :: {e.Message}");
                throw;
            }
        }
        /// <summary>
        /// Gets all Resource Ages for a particular resource
        /// </summary>
        /// <param name="resourceId">Resource Id</param>
        /// <returns>List of Resource Ages</returns>
        public List<Age> GetResourceAges(int resourceId)
        {
            try
            {
                var resourceAges = _context.ResourceAges.Where(x => x.ResourceId == resourceId).ToList();
                return resourceAges.Select(resourceAge => _context.Ages.FirstOrDefault(x => x.Id == resourceAge.AgeId))
                    .ToList();
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception getting Ages for Resource({resourceId}) :: GetResourceAges() :: {e.Message}");
                throw;
            }
        }
        /// <summary>
        /// Gets all Resource Insurances by Resource
        /// </summary>
        /// <param name="resourceId">Resource ID</param>
        /// <returns>List of ResourceInsurance</returns>
        public List<Insurance> GetResourceInsurances(int resourceId)
        {
            try
            {
                var resourceInsurances =  _context.ResourceInsurances.Where(x => x.ResourceId == resourceId).ToList();
                return resourceInsurances.Select(resourceInsurance =>
                    _context.Insurances.FirstOrDefault(x => x.Id == resourceInsurance.InsuranceId)).ToList();
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception getting Insurances for Resource({resourceId}) :: GetResourceInsurances() :: {e.Message}");
                throw;
            }
        }
        /// <summary>
        /// Gets all Resource Languages by Resource
        /// </summary>
        /// <param name="resourceId">Resource ID</param>
        /// <returns>List of ResourceLanguages</returns>
        public List<Language> GetResourceLanguages(int resourceId)
        {
            try
            {
                var resourceLanguages = _context.ResourceLanguages.Where(x => x.ResourceId == resourceId).ToList();
                return resourceLanguages.Select(resourceLanguage =>
                    _context.Languages.FirstOrDefault(x => x.Id == resourceLanguage.LanguageId)).ToList();
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception getting Languages for Resource({resourceId}) :: GetResourceLanguages() :: {e.Message}");
                throw;
            }
        }
        /// <summary>
        /// Gets all resource locations for a Resource
        /// </summary>
        /// <param name="resourceId">Resource Id</param>
        /// <returns>List of Resource Locations</returns>
        public List<Location> GetResourceLocations(int resourceId)
        {
            try
            {
                var resourceLocations =  _context.ResourceLocations.Where(x => x.ResourceId == resourceId).ToList();
                return resourceLocations.Select(resourceLocation =>
                    _context.Locations.FirstOrDefault(x => x.Id == resourceLocation.LocationId)).ToList();
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception getting Locations for Resource({resourceId}) :: GetResourceLocations() :: {e.Message}");
                throw;
            }
        }
        /// <summary>
        /// Gets All resource services for a given resource
        /// </summary>
        /// <param name="resourceId">Resource Id</param>
        /// <returns>List of Resource Services</returns>
        public List<Service> GetResourceServices(int resourceId)
        {
            try
            {
                var resourceServices =  _context.ResourceServices.Where(x => x.ResourceId == resourceId).ToList();
                return resourceServices.Select(resourceService =>
                    _context.Services.FirstOrDefault(x => x.Id == resourceService.ServicesId)).ToList();
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception getting Services for Resource({resourceId}) :: GetResourceServices() :: {e.Message}");
                throw;
            }
        }
        /// <summary>
        /// Get Resources Filtered by list of locations
        /// </summary>
        /// <param name="locationIds">Location Ids to include in the filter</param>
        /// <param name="resources">Filtered Resources</param>
        /// <returns>List of resources</returns>
        public List<Resource> GetResourcesByLocationFilter(List<int> locationIds, List<Resource> resources)
        {
            var model = new List<Resource>();
            try
            {
                foreach (var id in locationIds)
                {
                    var resourceLocations = _context.ResourceLocations.Where(x => x.LocationId == id).ToList();
                    model.AddRange(resourceLocations.SelectMany(x=>resources.Where(r=>r.Id==x.ResourceId).ToList()));
                }

                return model;
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception Getting Resources by Locations({JsonConvert.SerializeObject(locationIds)}) :: GetResourcesByLocationFilter() :: {e.Message}");
                throw;
            }
        }

        public List<Resource> GetResourcesByLocationFilter(List<int> locationIds)
        {
            try
            {
                var model = new List<Resource>();
                var rlList = new List<ResourceLocation>();
                foreach (var id in locationIds)
                {
                    rlList.AddRange(_context.ResourceLocations.Where(x => x.LocationId == id).ToList());
                }
                foreach (var rl in rlList)
                {
                    var resource = _context.Resources.Where(r=>r.Id==rl.ResourceId).ToList();
                    foreach (var r in resource)
                    {
                        model.Add(r);
                    }
                }

                return model;
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception Getting Resources by Locations({JsonConvert.SerializeObject(locationIds)}) :: GetResourcesByLocationFilter() :: {e.Message}");
                throw;
            }
        }
        /// <summary>
        /// Get Resources Filtered by list of Ages
        /// </summary>
        /// <param name="ageIds">Age ids for filtering</param>
        /// <param name="resourcesIn">Already filtered resources</param>
        /// <returns>list of resources</returns>
        public List<Resource> GetResourcesByAgeFilter(List<int> ageIds, List<Resource> resourcesIn)
        {
            try
            {
                var model = new List<Resource>();
                foreach (var id in ageIds)
                {
                    var providerAges = _context.ResourceAges.Where(x => x.AgeId == id).ToList();
                    model.AddRange(providerAges.SelectMany(x=> resourcesIn.Where(p=>p.Id==x.ResourceId).ToList()));
                }
                return model;
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception getting resources by Age({JsonConvert.SerializeObject(ageIds)}) :: GetResourcesByAgeFiler() :: {e.Message}");
                throw;
            }
        }
        public List<Resource> GetResourcesByAgeFilter(List<int> ageIds)
        {
            try
            {
                var model = new List<Resource>();
                var aList = new List<ResourceAge>();
                foreach (var id in ageIds)
                {
                    aList.AddRange(_context.ResourceAges.Where(x=>x.AgeId==id).ToList());
                }

                foreach (var a in aList)
                {
                    var resource = _context.Resources.Where(r => r.Id == a.ResourceId).ToList();
                    foreach (var r in resource)
                    {
                        model.Add(r);
                    }
                }
                
                return model;
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception getting resources by Age({JsonConvert.SerializeObject(ageIds)}) :: GetResourcesByAgeFiler() :: {e.Message}");
                throw;
            }
        }
        /// <summary>
        /// Gets a list of resources filtered by Insurance
        /// </summary>
        /// <param name="insuranceIds">List of Insurance Ids to filter by</param>
        /// <param name="resources">Filtered resources</param>
        /// <returns>List of resources</returns>
        public List<Resource> GetResourcesByInsuranceFilter(List<int> insuranceIds, List<Resource> resources)
        {
            try
            {
                var model = new List<Resource>();
                foreach (var id in insuranceIds)
                {
                    var resourceInsurances = _context.ResourceInsurances.Where(x => x.InsuranceId == id).ToList();
                    model.AddRange(resourceInsurances.SelectMany(x=>resources.Where(r=>r.Id==x.ResourceId).ToList()));
                }
                return model;
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception getting resources by Insurance({JsonConvert.SerializeObject(insuranceIds)}) :: GetResourcesByInsuranceFilter() :: {e.Message}");
                throw;
            }
        }
        public List<Resource> GetResourcesByInsuranceFilter(List<int> insuranceIds)
        {
            try
            {
                var model = new List<Resource>();
                var iList = new List<ResourceInsurance>();
                foreach (var id in insuranceIds)
                {
                    iList.AddRange(_context.ResourceInsurances.Where(x=>x.InsuranceId==id).ToList());
                }

                foreach (var i in iList)
                {
                    var resource = _context.Resources.Where(r => r.Id == i.ResourceId).ToList();
                    foreach (var r in resource)
                    {
                        model.Add(r);
                    }
                }
                return model;
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception getting resources by Insurance({JsonConvert.SerializeObject(insuranceIds)}) :: GetResourcesByInsuranceFilter() :: {e.Message}");
                throw;
            }
        }
        /// <summary>
        /// Gets a list of resources filtered by Languages
        /// </summary>
        /// <param name="languageIds">Language ids to filter by</param>
        /// <param name="resources">List of resource</param>
        /// <returns>List of resources</returns>
        public List<Resource> GetResourcesByLanguageFilter(List<int> languageIds, List<Resource> resources)
        {
            try
            {
                var model = new List<Resource>();
                foreach (var id in languageIds)
                {
                    var resourceLanguages = _context.ResourceLanguages.Where(x => x.LanguageId == id).ToList();
                    model.AddRange(resourceLanguages.SelectMany(x=>resources.Where(r=>r.Id==x.ResourceId).ToList()));
                }
                return model;
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception getting resources by Language({JsonConvert.SerializeObject(languageIds)}) :: GetResourcesByLanguageFilter() :: {e.Message}");
                throw;
            }
        }
        public List<Resource> GetResourcesByLanguageFilter(List<int> languageIds)
        {
            try
            {
                var model = new List<Resource>();
                var lList = new List<ResourceLanguage>();
                foreach (var id in languageIds)
                {
                    lList.AddRange(_context.ResourceLanguages.Where(x=>x.LanguageId==id).ToList());
                }

                foreach (var l in lList)
                {
                    var resource = _context.Resources.Where(r => r.Id == l.ResourceId).ToList();
                    foreach (var r in resource)
                    {
                        model.Add(r);
                    }
                }
                return model;
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception getting resources by Language({JsonConvert.SerializeObject(languageIds)}) :: GetResourcesByLanguageFilter() :: {e.Message}");
                throw;
            }
        }
        /// <summary>
        /// Gets a list of resources filtered by Service
        /// </summary>
        /// <param name="resourceIds">Resource ids to filter by</param>
        /// <param name="resources">filtered list of resources</param>
        /// <returns>list of resources</returns>
        public List<Resource> GetResourcesByServiceFilter(List<int> resourceIds, List<Resource> resources)
        {
            try
            {
                var model = new List<Resource>();
                foreach (var id in resourceIds)
                {
                    var resourceServices = _context.ResourceServices.Where(x => x.ServicesId == id).ToList();
                    model.AddRange(resourceServices.SelectMany(x => resources.Where(r => r.Id == x.ResourceId).ToList()));
                }
                return model;
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception getting resources by Services({JsonConvert.SerializeObject(resourceIds)}) :: GetResourcesByServiceFilter() :: {e.Message}");
                throw;
            }
        }
        public List<Resource> GetResourcesByServiceFilter(List<int> resourceIds)
        {
            try
            {
                var model = new List<Resource>();
                var sList = new List<ResourceService>();
                foreach (var id in resourceIds)
                {
                    sList.AddRange(_context.ResourceServices.Where(x=>x.ServicesId ==id).ToList());
                }
                foreach (var s in sList)
                {
                    var resource = _context.Resources.Where(r => r.Id == s.ResourceId).ToList();
                    foreach (var r in resource)
                    {
                        model.Add(r);
                    }
                }
                return model;
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception getting resources by Services({JsonConvert.SerializeObject(resourceIds)}) :: GetResourcesByServiceFilter() :: {e.Message}");
                throw;
            }
        }
    }
}
