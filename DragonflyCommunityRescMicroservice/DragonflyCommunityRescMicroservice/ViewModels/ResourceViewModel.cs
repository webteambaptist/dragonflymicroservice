﻿using System;
using System.Collections.Generic;
using DragonflyCommunityRescMicroservice.Models;

namespace DragonflyCommunityRescMicroservice.ViewModels
{
    public class ResourceViewModel
    {
        public ViewResource Resource { get; set; }
        public List<ViewLocation> LocationList { get; set; }
        public List<ViewInsurance> InsuranceList { get; set; }
        public List<ViewLanguage> LanguageList { get; set; }
        public List<ViewService> ServiceList { get; set; }
        public List<ViewAge> AgeList { get; set; }
    }
    
    public partial class ViewResource
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Website { get; set; }
        public string EntryUser { get; set; }
        public DateTime EntryDt { get; set; }
        public string UpdateUser { get; set; }
        public DateTime? UpdateDt { get; set; }
    }
    public partial class ViewAge
    {
        public int Id { get; set; }
        public string Age { get; set; }
    }
    public partial class ViewInsurance
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public partial class ViewLanguage
    {
        public int Id { get; set; }
        public string Language { get; set; }
    }
    public partial class ViewLocation
    {
        public int Id { get; set; }
        public string Location { get; set; }
        public string ZipCode { get; set; }
    }
    public partial class ViewService
    {
        public int Id { get; set; }
        public string Service { get; set; }
    }
}
