﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DragonflyCommunityRescMicroservice.Models;
using DragonflyCommunityRescMicroservice.Services;
using DragonflyCommunityRescMicroservice.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;
using NLog;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DragonflyCommunityRescMicroservice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DragonflyController : ControllerBase
    {
        private readonly Entity _entity;
        private readonly NLogger _logger;
        public DragonflyController(DragonflyContext context)
        {
            _entity = new Entity(context);
            _logger = new NLogger("DragonflyController");
        }
        /// <summary>
        /// Gets Locations
        /// </summary>
        /// <returns>list of locations</returns>
        [HttpGet]
        [Route("GetLocations")]
        [ProducesResponseType(StatusCodes.Status200OK, Type=typeof(List<ViewLocation>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Produces("application/json")]
        public IActionResult GetLocations()
        {
            try
            {
                var locations = _entity.GetLocations();
                return Ok(ConvertLocationToViewLocation(locations));
            }
            catch (Exception e)
            {
                var error = $"Exception occurred GetLocations() :: {e.Message}";
                _logger.WriteError(error);
                return BadRequest(error);
            }
        }
        /// <summary>
        /// Gets Ages
        /// </summary>
        /// <returns>list of ages</returns>
        [HttpGet]
        [Route("GetAges")]
        [ProducesResponseType(StatusCodes.Status200OK, Type=typeof(List<ViewAge>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Produces("application/json")]
        public IActionResult GetAges()
        {
            try
            {
                var ages = _entity.GetAges();
                return Ok(ConvertAgeToViewAge(ages));
            }
            catch (Exception e)
            {
                var error = $"Exception occurred GetAges() :: {e.Message}";
                _logger.WriteError(error);
                return BadRequest(error);
            }
        }
        /// <summary>
        /// Gets Insurances
        /// </summary>
        /// <returns>list of insurances</returns>
        [HttpGet]
        [Route("GetInsurances")]
        [ProducesResponseType(StatusCodes.Status200OK, Type=typeof(List<ViewInsurance>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Produces("application/json")]
        public IActionResult GeInsurances()
        {
            try
            {
                var insurances = _entity.GetInsurances();
                return Ok(ConvertInsuranceToViewInsurance(insurances));
            }
            catch (Exception e)
            {
                var error = $"Exception occurred GetInsurances() :: {e.Message}";
                _logger.WriteError(error);
                return BadRequest(error);
            }
        }
        /// <summary>
        /// Gets Languages
        /// </summary>
        /// <returns>list of languages</returns>
        [HttpGet]
        [Route("GetLanguages")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<ViewLanguage>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Produces("application/json")]
        public IActionResult GetLanguages()
        {
            try
            {
                var languages = _entity.GetLanguages();
                return Ok(ConvertLanguageToViewLanguage(languages));
            }
            catch (Exception e)
            {
                var error = $"Exception occurred GetLanguages() :: {e.Message}";
                _logger.WriteError(error);
                return BadRequest(error);
            }
        }
        /// <summary>
        /// Gets Services
        /// </summary>
        /// <returns>list of services</returns>
        [HttpGet]
        [Route("GetServices")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<ViewService>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Produces("application/json")]
        public IActionResult GetServices()
        {
            try
            {
                var services = _entity.GetServices();
                return Ok(ConvertServicesToViewServices(services));
            }
            catch (Exception e)
            {
                var error = $"Exception occurred GetServices() :: {e.Message}";
                _logger.WriteError(error);
                return BadRequest(error);
            }
        }

        [HttpGet]
        [Route("GetResources")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<Resource>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Produces("application/json")]
        public IActionResult GetResources()
        {
            try
            {
                if (!Request.Headers.ContainsKey("filters"))
                {
                    return NotFound("Missing Headers (filters)");
                }
                var filters = JsonConvert.DeserializeObject<Filters>(Request.Headers["filters"].ToString());
                if (filters == null) return NotFound("filters not found");
                var resources = new List<Resource>();
                if (filters.AgeFilters is not { Count: 0 })
                {
                    resources = resources is { Count: 0 } ? _entity.GetResourcesByAgeFilter(filters.AgeFilters) : _entity.GetResourcesByAgeFilter(filters.AgeFilters, resources);
                }

                if (filters.LocationFilters is not { Count: 0 })
                {
                    resources = resources is { Count: 0 } ? _entity.GetResourcesByLocationFilter(filters.LocationFilters) : _entity.GetResourcesByLocationFilter(filters.LocationFilters, resources);
                }

                if (filters.InsuranceFilters is not { Count: 0 })
                {
                    resources = resources is {Count:0} ? _entity.GetResourcesByInsuranceFilter(filters.InsuranceFilters) : _entity.GetResourcesByInsuranceFilter(filters.InsuranceFilters, resources);
                }

                if (filters.LanguageFilters is not { Count: 0 } )
                {
                    resources = resources is {Count:0} ? _entity.GetResourcesByLanguageFilter(filters.LanguageFilters) :_entity.GetResourcesByLanguageFilter(filters.LanguageFilters, resources);
                }

                if (filters.ServiceFilters is not { Count: 0 } )
                {
                    resources = resources is { Count:0 } ? _entity.GetResourcesByServiceFilter(filters.ServiceFilters) : _entity.GetResourcesByServiceFilter(filters.ServiceFilters, resources);
                }

                if (resources is { Count: 0 })
                {
                    return NotFound("No resources match that search criteria");
                }
                var viewModel = BuildViewModel(resources);

                return Ok(viewModel);

            }
            catch (Exception e)
            {
                var error = $"Exception :: GetResources() :: {e.Message}";
                _logger.WriteError(error);
                return BadRequest(error);
            }
        }

        private List<ViewAge> ConvertAgeToViewAge(IEnumerable<Age> ageList)
        {
            return ageList.Select(age => new ViewAge { Id = age.Id, Age = age.Age1 }).ToList();
        }

        private List<ViewLocation> ConvertLocationToViewLocation(IEnumerable<Location> locations)
        {
            return locations.Select(location => new ViewLocation { Id = location.Id, Location = location.Location1, ZipCode = location.ZipCode }).ToList();
        }

        private List<ViewInsurance> ConvertInsuranceToViewInsurance(IEnumerable<Insurance> insurances)
        {
            return insurances.Select(insurance => new ViewInsurance { Id = insurance.Id, Name = insurance.Name }).ToList();
        }

        private List<ViewLanguage> ConvertLanguageToViewLanguage(IEnumerable<Language> languages)
        {
            return languages.Select(language => new ViewLanguage { Id = language.Id, Language = language.Language1 }).ToList();
        }

        private List<ViewService> ConvertServicesToViewServices(IEnumerable<Service> services)
        {
            return services.Select(service => new ViewService { Id = service.Id, Service = service.Service1 }).ToList();
        }
        private IEnumerable<ResourceViewModel> BuildViewModel(List<Resource> resourceList)
        {
            var model = new List<ResourceViewModel>();
            try
            {
                foreach (var resource in resourceList)
                {
                    var r = new ResourceViewModel
                    {
                        Resource = new ViewResource()
                        {
                            Id=resource.Id,
                            Name = resource.Name,
                            PhoneNumber = resource.PhoneNumber,
                            Website = resource.Website,
                            EntryUser = resource.EntryUser,
                            EntryDt = resource.EntryDt,
                            UpdateUser = resource.UpdateUser,
                            UpdateDt = resource.UpdateDt
                        }
                    };

                    var ageList = _entity.GetResourceAges(resource.Id);
                    r.AgeList = ConvertAgeToViewAge(ageList);

                    var insuranceList = _entity.GetResourceInsurances(resource.Id);
                    r.InsuranceList = ConvertInsuranceToViewInsurance(insuranceList);

                    var languageList = _entity.GetResourceLanguages(resource.Id);
                    r.LanguageList = ConvertLanguageToViewLanguage(languageList);

                    var locationList = _entity.GetResourceLocations(resource.Id);
                    r.LocationList = ConvertLocationToViewLocation(locationList);

                    var serviceList = _entity.GetResourceServices(resource.Id);
                    r.ServiceList = ConvertServicesToViewServices(serviceList);

                    model.Add(r);
                }
                return model;
            }
            catch (Exception e)
            {
                var error = $"Exception :: BuildViewModel :: {e.Message}";
                _logger.WriteError(error);
                throw;
            }

        }
    }
}
